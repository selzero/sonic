﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sonic
{

    class LevelFromMeta
    {
        public List<ObjectsFromMeta> LevelTiles;
        public List<ObjectsFromMeta> LevelPipes;
    }
    class ObjectsFromMeta
    {
        public string ObjectType="";
        public float XPosition=0;
        public float YPosition=0;
    }
}
