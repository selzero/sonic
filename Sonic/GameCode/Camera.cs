﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Sonic
{
    class Camera2D    
    {        
        public Viewport viewport { get; set; }

        private float _zoom = 1f;
        public float Zoom
        {
            get { return _zoom; }
            set 
            { 
                _zoom = value;
                if (_zoom < 0.4f) _zoom = 0.4f;
                else if (_zoom > 1.5f) _zoom = 1.5f;
            } 
        }

        public Camera2D(Viewport Viewport)
        {
            viewport = Viewport;
            Bounds = new Rectangle(0,0,1,1);
        }

        public void Update(Character character)
        {
            Bounds = new Rectangle((int)(character.Position.X - 400 / _zoom), (int)(character.Position.Y - 300 / _zoom), (int)(800 / _zoom), (int)(600 / _zoom));

            var scaleMatrix = Matrix.CreateScale(new Vector3(_zoom, _zoom, 1));

            centre = new Vector2(character.Position.X-400 , 1/ _zoom);

            transform = Matrix.CreateRotationZ(rotation) * Matrix.CreateTranslation(-centre.X, -centre.Y, 0) * scaleMatrix;

            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fZoomValue">Use percentages.</param>
        public void setZoom(float fZoomValue)
        {
            Zoom += fZoomValue;
        }

        public Vector2 centre { get; set; }
        public Matrix transform { get; set; }
        public float rotation { get; set; }

        public Rectangle Bounds { get; set; }
    }
}


