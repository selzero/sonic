﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

using Microsoft.Xna.Framework.Content;

namespace Sonic
{
    class GameStatic
    {

        public static Random rndGen;
        public static Level CurrentLevel;
        public static Character PlayerCharacter;
        public static List<float[]> JumpCurve;


        public static void KillCharacter()
        {
            PlayerCharacter.Kill();
        }

        public static void CreateCurves()
        {
            rndGen = new Random();
            JumpCurve = new List<float[]>();

            float xStep = 0.1f;
            //create jump curve
            for (float x = 0f; x <= 1.0f; x += xStep)
            {
                float yValue = (float)(Math.Sin(((x / 2) * Math.PI) + (Math.PI / 2)));
                float[] f = new float[2];
                f[0] = x;
                f[1] = yValue;
                JumpCurve.Add(f);
            }
        }
    }
}
