﻿using HologramSpriteManager;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sonic
{
    class Bomb : AnimatedSprite
    {

        public bool bDestroyed = false;

        public Vector2 OriginalPosition;

        float fBombTime = 500;
        float iTotalY = 100;
        float iTotalX = 100;
        int iDirection = -1;
        public float fStartTime = 0;
        public Bomb(AnimatedSpriteSequences Meta)
            : base(Meta)
        {
            fStartTime = SpriteManager.GameTime;
            iTotalY= GameStatic.rndGen.Next(400)+10;
            iTotalX = GameStatic.rndGen.Next(400)+10;
            
        }

        public void Update()
        {

            if (bIsExploding && SpriteManager.GameTime - fStartExplosion > fExplosionDuration)
            {
                bDestroyed = true;
                return;
            }
            //get current time.
            float fCurrentTime = SpriteManager.GameTime;
            //Get time passed 
            float fTimePassed = fCurrentTime - fStartTime;
            if ((fTimePassed / fBombTime) > 1)
            {

                StartExplosion();
                return;
            }
            else
            {
                //get percentage time passed
                int iPassed = Convert.ToInt16(MathHelper.Lerp(0, 9, fTimePassed / fBombTime));
                //float fX = iTotalX * GameStatic.JumpCurve[iPassed][0] *iDirection;
                //Position.X = OriginalPosition.X + fX;
                    
                float iJumpAcceleration = GameStatic.JumpCurve[iPassed][1] *15f;
                Position = Position - (Vector2.UnitY * iJumpAcceleration);
            }

        }

        bool bIsExploding = false;
        float fStartExplosion = 0;
        float fExplosionDuration = 15;
        public void StartExplosion()
        {
            bIsExploding = true;
            fStartExplosion = SpriteManager.GameTime;
            ChangeAnimation("Explode");


            //bDestroyed = true;
            
        }

    }
}
