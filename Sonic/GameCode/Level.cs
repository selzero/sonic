﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using HologramSpriteManager;
using Newtonsoft.Json;

namespace Sonic
{
    class Level
    {
        public string Description;
        public List<AnimatedSprite> LevelTiles;
        public List<Pipe> LevelPipes;
        public List<Bomb> LevelBombs;

        public Level(string sFile)
        {
            LevelTiles = new List<AnimatedSprite>();
            LevelPipes = new List<Pipe>();
            LevelBombs = new List<Bomb>();
            PopulateLevelObjects(sFile);
            GameStatic.CurrentLevel = this;

        }

        public void Update()
        {
            for (int i = 0; i < LevelPipes.Count; i++)
            {
                LevelPipes[i].Update();
            }
            for (int i = 0; i < LevelBombs.Count; i++)
            {
                if(!LevelBombs[i].bDestroyed)
                    LevelBombs[i].Update();
            }
        }

        public void Draw()
        {
            for (int i = 0; i < LevelTiles.Count; i++)
            {
                LevelTiles[i].Draw();
            }

            for (int i = 0; i < LevelPipes.Count; i++)
            {
                LevelPipes[i].Draw();
            }

            for (int i = 0; i < LevelBombs.Count; i++)
            {
                if (!LevelBombs[i].bDestroyed)
                    LevelBombs[i].Draw();
            }

        }

        public bool CheckCollision(Rectangle rectangleToCheck)
        {

            foreach (var LevelObject in LevelTiles)
            {
                if (LevelObject.Bounds.Intersects(rectangleToCheck))
                {
                    //Console.WriteLine("Collide Tile!");
                    return true;
                }
            }
            foreach (var LevelObject in LevelPipes)
            {
                if (LevelObject.Bounds.Intersects(rectangleToCheck))
                {
                    //Console.WriteLine("Collide Pipe!");
                    return true;
                }
            }
            foreach (var LevelObject in LevelBombs)
            {
                if (!LevelObject.bDestroyed)
                {
                    if (LevelObject.Bounds.Intersects(rectangleToCheck))
                    {
                        //Console.WriteLine("Collide Bomb!!");
                        GameStatic.KillCharacter();
                        return true;
                    }
                }
            }
            return false;
        }
        private void PopulateLevelObjects(string sFile)
        {
            string sLevelText = System.IO.File.ReadAllText(sFile);

            LevelFromMeta lCurrentLevel = JsonConvert.DeserializeObject<LevelFromMeta>(sLevelText);
            for (int i = 0; i < lCurrentLevel.LevelTiles.Count; i++)
            {
                AnimatedSprite CurrentTile = AnimationSpecReader.PopulateAnimations<AnimatedSprite>(@".\Content\Specs\Tile.json");
                CurrentTile.Position.X = lCurrentLevel.LevelTiles[i].XPosition;
                CurrentTile.Position.Y = lCurrentLevel.LevelTiles[i].YPosition;
                LevelTiles.Add(CurrentTile);
            }                    
                    
            for (int i = 0; i < lCurrentLevel.LevelPipes.Count; i++)
            {
                Pipe CurrentPipe = AnimationSpecReader.PopulateAnimations<Pipe>(@".\Content\Specs\pipe.json");
                CurrentPipe.Position.X = lCurrentLevel.LevelPipes[i].XPosition;
                CurrentPipe.Position.Y = lCurrentLevel.LevelPipes[i].YPosition;
                LevelPipes.Add(CurrentPipe);
            }                    
        }



    }
}
