﻿using HologramSpriteManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sonic
{
    class Pipe : AnimatedSprite
    {
        public Pipe(AnimatedSpriteSequences Meta)
            : base(Meta)
        {
            
        }

        float fLastFireTime = 0;
        float fFirePause = 3000;
        public void Update()
        {
            if (fFirePause < SpriteManager.GameTime - fLastFireTime)
            {
                fLastFireTime = SpriteManager.GameTime;
                Fire();
            }
        }

        void Fire()
        {
            Bomb NewBomb = AnimationSpecReader.PopulateAnimations<Bomb>(@".\Content\Specs\bomb.json");
            NewBomb.Position = Position;
            NewBomb.Position.X += 40;
            //NewBomb.OriginalPosition = NewBomb.Position;
            GameStatic.CurrentLevel.LevelBombs.Add(NewBomb);


        }

    }
}
