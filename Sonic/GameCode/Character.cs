﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using HologramSpriteManager;

namespace Sonic
{
    class Character : AnimatedSprite
    {

        CharacterState CurrentCharacterState = CharacterState.idle;
        enum CharacterState
        { 
            idle,
            walking,
            running,
            jumping,
            hit,
            stop,
            falling,
            none
        }

        public Character(AnimatedSpriteSequences Meta) : base (Meta)
        {
            GameStatic.PlayerCharacter = this;
        }

        public void Update()
        {
            if (CurrentCharacterState == CharacterState.hit)
                return;


            GetInput();
            AffectWithGravity();
            EvaluateMove();
            EvaluateAnimation();
        }

        //the joystick controller
        private void GetInput()
        {
            GetDirection();


            if (IsOnFirmGround())
            {
                if (CurrentCharacterState == CharacterState.jumping)
                    CurrentCharacterState = CharacterState.idle;


            }

            if(GamePad.GetState(PlayerIndex.One).Buttons.A == ButtonState.Pressed)
            {
                if (!IsAirborne() && IsOnFirmGround())
                {
                    StartJump();
                }
                else
                {
                    //continue jump
                    if(CurrentCharacterState == CharacterState.jumping)
                        Jump();
                }
            }
        }

        private void EvaluateAnimation()
        {
            switch (CurrentCharacterState)
            {
                case CharacterState.hit:
                    ChangeAnimation("hit", false);
                    break;
                case CharacterState.jumping:
                    ChangeAnimation("jump",false);
                    break;
                case CharacterState.walking:
                    ChangeAnimation("walk", false);
                    break;
                case CharacterState.running:
                    ChangeAnimation("run", false);
                    break;
                case CharacterState.idle:
                    ChangeAnimation("idle", false);
                    break;
                case CharacterState.stop:
                    ChangeAnimation("stop", false);
                    break;
                case CharacterState.falling:
                    ChangeAnimation("jump", false);
                    break;
                case CharacterState.none:
                    ChangeAnimation("none", false);
                    break;
            }

        }

        float fJumpStartTime = 0;
        float fJumpMaxTime = 550;

        void StartJump()
        {
            //bJumpButtonDown = true;
            fJumpStartTime = SpriteManager.GameTime;
            CurrentCharacterState = CharacterState.jumping;
            Jump();
        }

        private void Jump()
        {
            if (CurrentCharacterState == CharacterState.jumping)
            {
                //get current time.
                float fCurrentTime = SpriteManager.GameTime;
                //Get time passed 
                float fTimePassed = fCurrentTime - fJumpStartTime;
                if (fTimePassed / fJumpMaxTime > 1)
                {
                    CurrentCharacterState = CharacterState.falling;
                    return;
                }
                //get percentage time passed
                int iPassed = Convert.ToInt16( MathHelper.Lerp(0,9,fTimePassed/fJumpMaxTime));

                int iJumpAcceleration = Convert.ToInt16(GameStatic.JumpCurve[iPassed][1] *15f);
                Movement -= (Vector2.UnitY * iJumpAcceleration);
            }
        }
        //walking / running
        float fMinWalk = 0.1f;
        float fCurrentWalk = 0;
        float fMaximumRun = 9f;
        float fRunAnim = 5f;
        float fStopMin = 1f;
        float fWalkStartTime;
        float fAcceleration = 0.1f;
        private void GetDirection()
        {
            float freactivity = 1f;
            Vector2 LeftStick = GamePad.GetState(PlayerIndex.One).ThumbSticks.Left;
            //get current direction;
            float fHorizontalDirection = LeftStick.X;
            if (fHorizontalDirection > 0.5f ) 
            {
                SetEffect(SpriteEffects.None);
                if (fCurrentWalk < -1 * fMinWalk)
                    freactivity = 0.95f;

                fCurrentWalk += (fAcceleration * freactivity);
                

                if (fCurrentWalk < fMinWalk * -1) //pull joypad right while player is moving left:
                {
                    fCurrentWalk = (fCurrentWalk < fMaximumRun * -1) ? fMaximumRun * -1 : fCurrentWalk;
                    if (!IsAirborne())
                    {
                        if (fCurrentWalk * -1 > fStopMin)
                        {
                            CurrentCharacterState = CharacterState.stop;
                        }
                        else
                        {
                            CurrentCharacterState = CharacterState.walking;
                        }
                    }
                }
                else //pull joypad right while player is moving right or standing still
                {
                    fCurrentWalk = (fCurrentWalk < fMinWalk) ? fMinWalk : (fCurrentWalk > fMaximumRun) ? fMaximumRun : fCurrentWalk;

                    if (fCurrentWalk < fRunAnim && !IsAirborne())
                    {
                        CurrentCharacterState = CharacterState.walking;
                    }
                    if (fCurrentWalk > fRunAnim && !IsAirborne())
                    {
                        CurrentCharacterState = CharacterState.running;
                    }
                }
            }
            else if (fHorizontalDirection < -0.5f )
            {
                SetEffect(SpriteEffects.FlipHorizontally);

                if (fCurrentWalk > fMinWalk)
                    freactivity = 0.95f;
                fCurrentWalk -= (fAcceleration * freactivity);
                if (fCurrentWalk > fMinWalk)
                {
                    if (!IsAirborne())
                    {
                        fCurrentWalk = (fCurrentWalk > fMaximumRun) ? fMaximumRun : fCurrentWalk;
                        if (fCurrentWalk > fStopMin)
                            CurrentCharacterState = CharacterState.stop;
                        else
                            CurrentCharacterState = CharacterState.walking;
                    }
                }
                else
                {
                    fCurrentWalk = (fCurrentWalk > fMinWalk * -1) ? fMinWalk * -1 : (fCurrentWalk < fMaximumRun * -1) ? fMaximumRun * -1 : fCurrentWalk;

                    if (fCurrentWalk * -1 < fRunAnim && !IsAirborne())
                    {
                        CurrentCharacterState = CharacterState.walking;
                    }
                    if (fCurrentWalk * -1 > fRunAnim && !IsAirborne())
                    {
                        CurrentCharacterState = CharacterState.running;
                    }
                }
            }
            else
            {
                if (fCurrentWalk == 0)
                {
                    fWalkStartTime = SpriteManager.GameTime;

                    if (!IsAirborne())
                    {
                        CurrentCharacterState = CharacterState.idle;
                    }
                }
                else
                    freactivity = 0.8f;

                if(fCurrentWalk > 1)
                    fCurrentWalk -= (fAcceleration *  freactivity);
                else if (fCurrentWalk < -1)
                    fCurrentWalk += (fAcceleration *  freactivity);
                else
                {
                    fCurrentWalk = 0;
                    freactivity = 1;
                }

            }
            
            Movement = new Vector2(fCurrentWalk, 0);
        }


        bool IsAirborne()
        {
            if (CurrentCharacterState == CharacterState.jumping || CurrentCharacterState == CharacterState.falling)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void EvaluateMove()
        {
            Vector2 OldPosition = Position;
            Move();

            Position = WhereCanIGetTo(OldPosition, Position, Bounds);
        }

        private void Move()
        {
            Position += Movement;
            
        }

        private void AffectWithGravity()
        {
            Movement += Vector2.UnitY * 5f;
        }

        public Vector2 WhereCanIGetTo(Vector2 originalPosition, Vector2 destination, Rectangle boundingRectangle)
        {
            Vector2 movementToTry = destination - originalPosition;
            Vector2 furthestAvailableLocationSoFar = originalPosition;
            int numberOfStepsToBreakMovementInto = (int)(movementToTry.Length() * 2) + 1;
            Vector2 oneStep = movementToTry / numberOfStepsToBreakMovementInto;

            for (int i = 1; i <= numberOfStepsToBreakMovementInto; i++)
            {
                Vector2 positionToTry = originalPosition + oneStep * i;
                Rectangle newBoundary = CreateRectangleAtPosition(positionToTry, boundingRectangle.Width, boundingRectangle.Height);
                if (!GameStatic.CurrentLevel.CheckCollision(newBoundary)) 
                {
                    furthestAvailableLocationSoFar = positionToTry; 
                }
                else
                {

                    bool isDiagonalMove = movementToTry.X != 0 && movementToTry.Y != 0;
                    if (isDiagonalMove)
                    {
                        int stepsLeft = numberOfStepsToBreakMovementInto - (i - 1);

                        Vector2 remainingHorizontalMovement = oneStep.X * Vector2.UnitX * stepsLeft;
                        Vector2 finalPositionIfMovingHorizontally = furthestAvailableLocationSoFar + remainingHorizontalMovement;
                        furthestAvailableLocationSoFar = WhereCanIGetTo(furthestAvailableLocationSoFar, finalPositionIfMovingHorizontally, boundingRectangle);

                        Vector2 remainingVerticalMovement = oneStep.Y * Vector2.UnitY * stepsLeft;
                        Vector2 finalPositionIfMovingVertically = furthestAvailableLocationSoFar + remainingVerticalMovement;
                        furthestAvailableLocationSoFar = WhereCanIGetTo(furthestAvailableLocationSoFar, finalPositionIfMovingVertically, boundingRectangle);
                    }
                    break;
                }
            }
            return furthestAvailableLocationSoFar;
        }

        public bool IsOnFirmGround()
        {

            if (CurrentCharacterState == CharacterState.falling)
            {
                int a = 0;
            }
            Rectangle onePixelLower = Bounds;
            onePixelLower.Offset(0, 1);
            bool bRet =  GameStatic.CurrentLevel.CheckCollision(onePixelLower);
            if (bRet && IsAirborne())
                CurrentCharacterState = GetLandBasedState();
            return bRet;
        }

        CharacterState GetLandBasedState()
        {
            if (fCurrentWalk > -0.5f && fCurrentWalk < 0.5f)
                return CharacterState.idle;
            else if (fCurrentWalk > fRunAnim || fCurrentWalk < -1 * fRunAnim)
                return CharacterState.running;
            else
                return CharacterState.walking;

        }

        public void Kill()
        {
            CurrentCharacterState = CharacterState.hit;
        }

        private Rectangle CreateRectangleAtPosition(Vector2 positionToTry, int width, int height)
        {
            return new Rectangle((int)positionToTry.X, (int)positionToTry.Y, width, height);
        }
    }
}
