﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;

using HologramSpriteManager;

#endregion

namespace Sonic
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class SonicGame : Game
    {
        GraphicsDeviceManager graphics;
        //SpriteBatch spriteBatch;
        public static Texture2D DebugPoint;
        private Texture2D background;        
        //private Character Orbit;
        private Character Sonic;
        Camera2D _camera;

        public SonicGame() : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            SpriteManager.ContentShell = Content;
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            _camera = new Camera2D(this.GraphicsDevice.Viewport);

            GameStatic.CreateCurves();
            base.Initialize();
        }


        Bomb test;

        protected override void LoadContent()
        {
            SpriteManager.GameTime = 0;
            // Create a new SpriteBatch, which can be used to draw textures.
            SpriteManager.spriteBatch = new SpriteBatch(GraphicsDevice);
            background = Content.Load<Texture2D>("Sprites/water");
            Texture2D TileTexture = Content.Load<Texture2D>("Sprites/tile");
            Sonic = AnimationSpecReader.PopulateAnimations<Character>(@".\Content\Specs\SONIC.json");
            Sonic.Position = new Vector2(400, 360);
            GameStatic.CurrentLevel = new Level(@".\Content\Specs\Level1.json");
        }
        protected override void UnloadContent()
        {
        }
        protected override void Update(GameTime gameTime)
        {
            SpriteManager.GameTime += (float)gameTime.ElapsedGameTime.TotalMilliseconds; 

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            GameStatic.CurrentLevel.Update();
            Sonic.Update();
            _camera.Update(Sonic);
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            //_camera = new Camera2D(this.GraphicsDevice.Viewport);
            SpriteManager.spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, _camera.transform);
            //SpriteManager.spriteBatch.Begin();

            Sonic.Draw();
            GameStatic.CurrentLevel.Draw();

            SpriteManager.spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
